module Gitlab
  module Homepage
    autoload :Team, 'lib/team'

    class Team
      autoload :Member, 'lib/team/member'
      autoload :Project, 'lib/team/project'
      autoload :Assignment, 'lib/team/assignment'
    end
  end
end
