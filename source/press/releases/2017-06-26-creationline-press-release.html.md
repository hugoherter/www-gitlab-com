---
layout: markdown_page
title: "GitLab add Creationline to Global Partnership Program"
---

**San Francisco (June 26, 2017)** – Today, GitLab announced a partnership with Creationline Inc., a Tokyo-based company providing solutions for cloud integration, microservices, IoT, big data, automation, and DevOps. Creationline will now sell and support GitLab products as part of their portfolio. Additionally, Creationline provides a Japanese support desk for GitLab Enterprise Edition customers. Creationline can handle customers' owned GitLab server as a managed service to remove operational overhead and allow IT teams to focus on more valuable tasks. Ultimately, Creationline's expertise in cloud integration, DevOps, big data, and microservices will not only help enterprise development teams get started using GitLab, it will also support teams in their efforts to achieve shorter development cycles.

“In recent years, we have observed an increased interest in Agile and DevOps practices from all industries in Japan, and now large organizations have started to embrace such movements. GitLab's aggregation of source code control, collaboration, and CI/CD into one solution can address the need for our customers to implement efficient product development pipelines. With GitLab, our customers will be able to achieve continuous integration, continuous development, monitoring and feedback cycles without having to worry about the extra overhead of managing multiple tools. In addition to GitLab’s product offering, we chose to work with the company for its high-paced growing product and services and for its culture of transparency and knowledge-sharing with their organization and towards the open source community,” says Creationline Inc. CEO, Yasuda Tadahiro. 

“Creationline brings over a decade of experience working with enterprise companies. Their experience paired with GitLab’s product and mission to provide an integrated software development platform will enable large enterprises to meet their DevOps, microservices, or cloud transformation goals. As a global company, with employees and customers spread across the world, GitLab is always looking to work with partners that are able to provide sales and support/services in local markets. We are excited to work with Creationline to help make GitLab more accessible to enterprise development teams in Japan and across Asia,” says Michael Alessio, GitLab’s Director of Global Alliances.  

Creationline Inc. joins a growing community of GitLab partners, around the world. For a complete list of GitLab resellers, please visit  [https://about.gitlab.com/resellers/](https://about.gitlab.com/resellers/) 

**About Creationline Inc.**

Founded in 2006, Creationline Inc. is Japanese company providing solutions for cloud integration, microservices, IoT, big data, automation, and DevOps. We have strong expertise working with enterprise customers and creating new ways (technologies, services, business) to bring customers more value without being bound by the existing concept/information base. 

For more information about Creationline Inc., please visit:

* [Creationline website](https://www.creationline.com/)
* [GitLab-specific webpage on Creationline website](https://www.creationline.com/GitLab/) 
* [Creationline Facebook page](https://www.facebook.com/creationline/)

**About GitLab**

Since its founding in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring platform that accelerates and simplifies the software development process. With one end-to-end software development platform, GitLab helps teams eliminate unnecessary steps from their workflow and focus exclusively on building great software. Today, more than 100,000 organizations, including NASA, CERN, Alibaba, SpaceX, O'Reilly, IBM and ING, trust GitLab to bring their modern applications from idea to production, reliably and repeatedly.